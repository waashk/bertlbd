from sys import argv
import json
import os
import numpy as np
from sklearn.datasets import load_svmlight_file, dump_svmlight_file
import argparse
from scipy.stats import t as qt
from os import path
from sklearn.preprocessing import LabelEncoder
import io
import random


import re
import nltk
from collections import Counter, defaultdict
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split, StratifiedShuffleSplit

from glob import glob
import pandas as pd
import numpy as np
import copy

stop_words = set(stopwords.words('english'))


def get_array(X, idxs):
	return [ X[idx] for idx in idxs ]
def readfile(filename):
	with io.open(filename, 'rt', newline='\n', encoding='utf8', errors='ignore') as filein:
		return filein.readlines()
def load_splits_ids(folddir):
	splits = []
	with open(folddir, encoding='utf8', errors='ignore') as fileout:
		#a=1
		for line in fileout.readlines():
			#print(a)
			#a+=1
			aux = line.split(';')
			#print(len(aux))
			train_index, test_index = line.split(';')
			train_index = list(map(int, train_index.split()))
			test_index = list(map(int, test_index.split()))
			splits.append( (train_index, test_index) )
	return splits

def load_splits_ids_with_validation(folddir):
    splits = []
    with open(folddir, encoding='utf8', errors='ignore') as fileout:
        #a=1
        for line in fileout.readlines():
            #print(a)
            #a+=1
            aux = line.split(';')
            #print(len(aux))
            train_index, valid_index, test_index = line.split(';')
            train_index = list(map(int, train_index.split()))
            valid_index = list(map(int, valid_index.split()))
            test_index = list(map(int, test_index.split()))
            #valid_index = list(map(int, valid_index.split()))
            #splits.append( (train_index, test_index, valid_index) )
            splits.append( (train_index, valid_index, test_index) )
    return splits


def stats(nfolds, micro_list, macro_list):
	med_mic = np.mean(micro_list)*100
	error_mic = abs(qt.isf(0.975,df=(nfolds-1)))*np.std(micro_list,ddof=1)/np.sqrt(len(micro_list))*100
	med_mac = np.mean(macro_list)*100
	error_mac = abs(qt.isf(0.975,df=(nfolds-1)))*np.std(macro_list,ddof=1)/np.sqrt(len(macro_list))*100
	#print("Micro\tMacro")
	print("{:.2f}\t{:.2f}\t{:.2f}\t{:.2f}\t".format(med_mic,error_mic,med_mac,error_mac),end="")
	#args['stats'] = "Micro={:.2f}({:.2f}) Macro={:.2f}({:.2f})".format(med_mic,error_mic,med_mac,error_mac)

def createOutputPath(finaloutput):
	if not path.exists(finaloutput):
		os.makedirs(finaloutput)






def clean_str(string):
	"""
	Tokenization/string cleaning for all datasets except for SST.
	Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
	"""
	string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
	string = re.sub(r"\'s", " \'s", string)
	string = re.sub(r"\'ve", " \'ve", string)
	string = re.sub(r"n\'t", " n\'t", string)
	string = re.sub(r"\'re", " \'re", string)
	string = re.sub(r"\'d", " \'d", string)
	string = re.sub(r"\'ll", " \'ll", string)
	string = re.sub(r",", " , ", string)
	string = re.sub(r"!", " ! ", string)
	string = re.sub(r"\(", " \( ", string)
	string = re.sub(r"\)", " \) ", string)
	string = re.sub(r"\?", " \? ", string)
	string = re.sub(r"\s{2,}", " ", string)
	return string.strip().lower()


def clean_doc(x, word_freq):
	clean_docs = []
	most_commons = dict(word_freq.most_common(min(len(word_freq), 50000)))
	for doc_content in x:
		doc_words = []
		cleaned = clean_str(doc_content.strip())
		for word in cleaned.split():
			if word not in stop_words and word_freq[word] >= 5:
				if word in most_commons:
					doc_words.append(word)
				else:
					doc_words.append("<UNK>")
		doc_str = ' '.join(doc_words).strip()
		clean_docs.append(doc_str)
	return clean_docs

def duplicate(X, y):
	#q = []
	q1 = []

	y_set = list(sorted(list(set(y))))
	mydict = Counter(y)

	for i in mydict.keys():
		if mydict[i] == 1:
			q1.append(i)
	
	#print(dict(Counter(y)).index(1))

	#print(y_set)
	#q = [len(np.where(y == i)[0]) for i in y_set]
	
	#q1 = np.where(np.asarray(q) == 1)[0]
	#print(q1)
	#print(np.where(y == 1))
	#exit()
	#print(X.shape)

	X_res = copy.copy(X)
	y_res = copy.copy(y)

	if len(q1)>0:
		print("duplicando")
		for q in q1:
			print("duplicando", q)
			#print(np.where(y_res == q))
			q2 = np.where(y_res == q)[0][0]
			#print(q2)
			y_res = np.append(y_res,y_res[q2])
			#print(len(y_res))
	
			#X_res.data = np.hstack((X_res.data,X_res[q2].data))
			#X_res.indices = np.hstack((X_res.indices,X_res[q2].indices))
			#X_res.indptr = np.hstack((X_res.indptr,(X_res[q2].indptr + X_res.nnz)[1:]))
			#X_res._shape = (X_res.shape[0]+1,X_res[q2].shape[1])
			X_res.append(X_res[q2])

	return X_res, y_res

def prep_data(X_train, y_train, X_test, y_test, cleaning_doc=True,preprocess=True):
	#if not os.path.exists(working_dir):
	#    os.makedirs(working_dir)
	X_train, y_train = duplicate(X_train, y_train)

	if preprocess:
		if cleaning_doc:
			word_freq = Counter()  # to remove rare words
			for text in X_train:
				for word in clean_str(text.strip()).split():
				#for word in nltk.word_tokenize(text.lower()):
					if word in word_freq:
						word_freq[word] += 1
					else:
						word_freq[word] = 1
			X_train = clean_doc(X_train, word_freq)
			X_test = clean_doc(X_test, word_freq)
		else: # basic cleaning
			X_train = [clean_str(_) for _ in X_train]
			X_test = [clean_str(_) for _ in X_test]

	#X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.15, random_state=1)
	sss = StratifiedShuffleSplit(n_splits=2, test_size=0.1, random_state=2018)
	for train_index, val_index in sss.split(X_train, y_train):
		continue
	
	X_train = np.asarray(X_train)
	X_test = np.asarray(X_test)

	y_train = np.asarray(y_train)
	y_test = np.asarray(y_test)

	#print(train_index)

	X_train_new = X_train[train_index]
	y_train_new = y_train[train_index]
	X_val   = X_train[val_index]
	y_val   = y_train[val_index]

	return X_train_new, y_train_new, X_test, y_test, X_val, y_val


def prep_data2(X_train, y_train, X_test, y_test, X_val, y_val, cleaning_doc=True):
	#if not os.path.exists(working_dir):
	#    os.makedirs(working_dir)
	if cleaning_doc:
		word_freq = Counter()  # to remove rare words
		for text in X_train:
			for word in clean_str(text.strip()).split():
			#for word in nltk.word_tokenize(text.lower()):
				if word in word_freq:
					word_freq[word] += 1
				else:
					word_freq[word] = 1
		for text in X_val:
			for word in clean_str(text.strip()).split():
			#for word in nltk.word_tokenize(text.lower()):
				if word in word_freq:
					word_freq[word] += 1
				else:
					word_freq[word] = 1
		X_train = clean_doc(X_train, word_freq)
		X_test = clean_doc(X_test, word_freq)
		X_val = clean_doc(X_val, word_freq)
	else: # basic cleaning
		X_train = [clean_str(_) for _ in X_train]
		X_test = [clean_str(_) for _ in X_test]
		X_val = [clean_str(_) for _ in X_test]
	
	#X_train = np.asarray(X_train)
	#X_test = np.asarray(X_test)
	#X_val = np.asarray(X_val)

	#y_train = np.asarray(y_train)
	#y_test = np.asarray(y_test)
	#y_val = np.asarray(y_val)

	return X_train, y_train, X_test, y_test, X_val, y_val


