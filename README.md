# Executando BERT

## Comando:


## Argumentos:

```
dataset: Nome do dataset presente no diretório datasets
	Dentro do diretório existem os seguintes arquivos:
		datasets/<dataset>/texts.txt: Conjunto de documentos (texto). Um por linha.
		datasets/<dataset>/score.txt: Classe do documento cujo indece é associado ao texts.txt
		datasets/<dataset>/split_<k>.csv: 
		Se dev=0:
			Arquivo com k linhas, com a divisão treino e teste.
			Exemplo
				idx0_treino idx1_treino ; idx2_teste
				idx2_treino idx1_treino ; idx3_teste
				idx1_treino idx3_treino ; idx4_teste
				idx0_treino idx4_treino ; idx1_teste
		
		Se dev=1:
			Arquivo com k linhas, com a divisão treino, teste e validação.
			Exemplo:
				idx0_treino idx1_treino ; idx2_teste ; idx3_val
				idx2_treino idx1_treino ; idx3_teste ; idx4_val
				idx1_treino idx3_treino ; idx4_teste ; idx2_val
				idx0_treino idx4_treino ; idx1_teste ; idx3_val

nfolds: Número de folds a ser considerado.
	Cuidado pois esse argumento que seleciona o arquivo split_<k>.csv

foldesp: Realiza o processo em um fold especifico ou em todos
	default=-1: realiza o processo em todos os folds
	k >=0: realiza o processo no fold k

out: diretorio onde é salvo a saida 
	default="saida/"

preprocess: Realizar preprocessamento?
	default=0

cleaning_doc: preprocessamento maior?
	default=False

dev: Partição (treino e teste) ou (treino, dev e teste) 
	default=0: split_<k>.csv somente com partição treino e teste;
	dev=1: split_<k>.csv com partição treino, teste e validação;

max_len: tamanho maximo (em número de tokens) a ser considerado.
	default=100

batch_size: Quantidade de documentos por batch
	default=16
```


# Instalação do CUDA, DOCKER e Preparando IMAGEM

## Informações importantes:

	- É importante que vocês tenham as versões corretas do CUDA e CUDNN na máquina, pois a camada do cuda é abaixo da do Docker (Foi essa parte que lutei durante muito tempo rsrs)
	- Minha solução pra isso então é removermos a versão atual (Se não for uma das citadas abaixo) e instalarmos a versão que precisamos. Logo após executarmos os experimentos, voltarmos para a versão anterior (pois o amiguinho do lab pode precisar da que já estava lá). Vejo esse passo como importante pois sabemos que eventualmente cada pessoa usa uma versao CUDA diferente (pois existe dependencia especifica do tensorflow, mxnet, pytorch e etc...)
	- Atenção: Muito importante conversar com todas as pessoas que utilizam determinada maquina (especialmente quando estão alocadas), pois isso influencia diretamente nos resultados e execução de algoritmos.
	- Além disso, toda vez que acabar de usar uma máquina, favor voltar a versão que se encontrava antes (explicarei os passos pra isso logo mais.)

	- Para o algoritmo do BERT eu testei e deu certo o CUDA 10.0, 10.1 e 10.2. Já o CUDA 11.0 não deu certo.
	- Para o algoritmo do BERT eu testei e deu certo o CUDNN XXX. Testei outros e deu erro porém não salvei as versões (minhas desculpas rs)

	- Estou trabalhando em uma forma de mantermos multiplas versões do CUDA em uma máquina e referenciarmos a atual antes da criação do Docker. Porém isso é bem dificultoso (existem algumas limitações do cuda toolkit e por isso demorei tanto tempo para disponibilizar essa versão). Se alguém ai tiver experiencia por favor me avise, pois assim podemos trabalhar juntos nessa solução.

## 1) Versão do CUDA e CUDNN no servidor fora do Docker:

### 1.1) Verificando se existe algum cuda tookit instalado

```
nvidia-smi
```

No header deve mostrar algo do tipo:

> NVIDIA-SMI 418.152.00   Driver Version: 418.152.00   CUDA Version: 10.1 

- Se der erro o nvidia-smi, tente o comando: 

```
ls /usr/local/
```

- Se não tiver nenhuma pasta cuda, provavelmente a máquina está limpa, então pule para o passo 1.4 

### 1.2) Salvando versões atuais e configurações do CUDA

Com o commando nvidia-smi já temos as seguintes informações:

- 			Toolkit Version: 418.152.00 
- 			Driver Version: 418.152.00   
- 			CUDA Version: 10.1

		
#### Vamos tratar variavél PATH. 

```
echo $PATH
```

- no meu caso retornou: 

> /usr/local/cuda/bin:/usr/local/cuda-10.1/bin:/home/waashk/bin:/home/waashk/.local/bin:/home/waashk/miniconda/bin:/home/waashk/miniconda/condabin:/usr/local/cuda-10.0/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

- no caso dessa máquina já tem o cuda-10.1 (o que mostrou mesmo no comando nvidia-smi)

- Vamos salvar a variavel path em outra variavel, para possibilitar que possamos voltar com ambiente anterior após os nossos experimentos

```
export OLDPATH=`echo $PATH`
```

- Informações adicionais (somente para conhecimento): vejam que também mostrou que tem o cuda-10.0, mas não tem cuda-10.0 no diretorio /usr/local/, então provavelmente desinstalaram esse cuda dessa máquina e manteram na variavel $PATH. Sendo assim, poderiamos retirar a parte que fala do cuda-10.1 que continuaria funcionando normalmente.

#### Vamos tratar a variavel LD_LIBRARY_PATH

```
echo $LD_LIBRARY_PATH
```

- no meu caso retornou: 

> /usr/local/cuda/lib64:/usr/local/cuda-10.1/lib64:/usr/local/cuda-10.0/lib64

- mesmo comentário que o caso anterior. Vejam que havia o cuda-10.0 e não removeram nessa variável também

- Vamos salvar a variavel path em outra variavel, para possibilitar que possamos voltar com ambiente anterior após os nossos experimentos

```
export OLDLD_LIBRARY_PATH=`echo $LD_LIBRARY_PATH`
``` 


###	1.3) Desinstale o Cuda atual

```
sudo apt-get purge nvidia*

sudo apt-get autoremove

sudo apt-get autoclean

sudo rm -rf /usr/local/cuda*
```

Remova também as referencias nas variaveis de ambiente PATH e LD_LIBRARY_PATH

```
export PATH="/home/waashk/bin:/home/waashk/.local/bin:/home/waashk/miniconda/bin:/home/waashk/miniconda/condabin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"

export LD_LIBRARY_PATH=""
```

> PS: Lembre que esse foi meu caso especifico, então peguem o que tinha na variavel de vocês e só removam o que tem a ver com o CUDA.


###	1.4) Instalando Cuda Toolkit 10.2 

- Vou instalar essa versão pois foi a mais otimizada para o BERT que eu utilizei e, além disso, todas as placas de video do lab (atualmente) suportam essa versão.

#### 1.4.1) Verifique versao do Ubuntu

`lsb_release -a`
			
> Distributor ID:	Ubuntu
> Description:	Ubuntu 16.04.4 LTS
> Release:	16.04
> Codename:	xenial


#### 1.4.2) Verifique arquitetura e placa gpu na maquina:

`uname -a`

> Linux galapagos 4.4.0-190-generic #220-Ubuntu SMP Fri Aug 28 23:02:15 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux

`lspci | grep -i --color 'vga\|3d\|2d'`

> **01:00.0** VGA compatible controller: NVIDIA Corporation GV100 (rev a1). A marcação em negrito mostra a informação que usaremos a seguir.

`sudo lspci -v -s 01:00.0`

> 01:00.0 VGA compatible controller: NVIDIA Corporation GV100 (rev a1) (prog-if 00 [VGA controller])
Subsystem: NVIDIA Corporation GV100 [TITAN V]

Com essas informações você já estará apto a prosseguir nos proximos passos.

#### 1.4.3) Verifique se o gcc está instalado: 

`gcc --version`

Se der erro, baixe com o seguinte comando:

`sudo apt install build-essential`

#### 1.4.4) Baixe o Cuda toolkit para sua versão e arquitetura:

Entre no link para escolher a versão cuda toolkit:

```https://developer.nvidia.com/cuda-toolkit-archive``

Nesse caso estou utilizando o seguinte link: http://developer.download.nvidia.com/compute/cuda/10.2/Prod/local_installers/cuda_10.2.89_440.33.01_linux.run

- O 440.33.01 é a versão do toolkit. Veja nesse link https://docs.nvidia.com/deploy/cuda-compatibility/index.html se o cuda anterior tem compatibilidade com esse toolkit novo. Pois isso pode interferir nos experimentos alheios.

- Se tiver tudo ok, baixe com:

`wget http://developer.download.nvidia.com/compute/cuda/10.2/Prod/local_installers/cuda_10.2.89_440.33.01_linux.run`


#### 1.4.5) Instalando Cuda Driver antes do toolkit: 

Se o passo anterior reclamar do Cuda Driver, faça esse passo. Se não, siga para o 1.4.6.

Para esse passo é necessario entrar no seguinte link e encontrar o Driver desejado:

```
https://www.nvidia.com.br/Download/index.aspx?lang=br
```

> No meu caso retornou o seguinte link: https://us.download.nvidia.com/tesla/440.33.01/nvidia-driver-local-repo-ubuntu1804-440.33.01_1.0-1_amd64.deb

```
wget https://us.download.nvidia.com/tesla/440.33.01/nvidia-driver-local-repo-ubuntu1804-440.33.01_1.0-1_amd64.deb

sudo apt-key add /var/nvidia-driver-local-repo-440.33.01/7fa2af80.pub

sudo apt-get update

sudo dpkg -i nvidia-driver-local-repo-ubuntu1804-440.33.01_1.0-1_amd64.deb
```


#### 1.4.6) Instale o Cuda toolkit.

- Essa é a parte que pra mim já deu mais problemas rsrs Porque tava tentando manter mais de uma versão ao mesmo tempo.

- Nessa parte é seguir as etapas de instalação (torcendo pra dar certo kkkk). 

`sudo sh cuda_10.2.89_440.33.01_linux.run`

#### 1.4.7) Linkando nova versão do CUDA

command:

```
sudo ln -s /usr/local/cuda-10.2/ /usr/local/cuda/    

export PATH=/usr/local/cuda/bin:/usr/local/cuda-10.2/bin:$PATH

export LD_LIBRARY_PATH=/usr/local/cuda/lib64:/usr/local/cuda-10.2/lib64:$LD_LIBRARY_PATH
```

- Nesse ponto tente ver se deu tudo certo com: nvidia-smi e confira as versões

###	1.4) Instalando CuDNN XX

- Essa parte é bem chatinha também, porque demanda pegar a versão especifica no site da nvidia. Isso demanda login e senha. (Bypass que eu implementei não deu certo)

- Portanto, vou manter um tar.gz com a versão que vamos usar na máquina hidra. (diretorio: /home/lbduser/cudnn/cudnn-10.2-linux-x64-v8.0.3.33.tgz)

- Se não tiver lá, façam o download no site da nvidia procurando por essa versão ou me mandem um email que eu baixo novamente e insiro na hidra. 

```
tar -xzvf cudnn-10.2-linux-x64-v8.0.3.33.tgz

sudo cp cuda/include/cudnn.h /usr/local/cuda-10.2/include

sudo cp cuda/lib64/libcudnn* /usr/local/cuda-10.2/lib64

sudo chmod a+r /usr/local/cuda-10.2/include/cudnn.h /usr/local/cuda-10.2/lib64/libcudnn*
```
## 2) Instalando virtualenv


**IMPORTANTE**: Verificando requeriments.txt: 

- Se você escolheu outra versão do cuda, edite a ultima linha do arquivo requeriments.txt 
Deixei como default: mxnet-cu102mkl. Atualmente eu verifiquei que existem versões mxnet-cu100mkl e mxnet-cu101mkl.

Baixe o pacote pelo seguinte link: (Ainda não disponivel no pip na versão 1.7.0)

`wget https://files.pythonhosted.org/packages/46/a4/7c81a3ddd2d406bd1e13aa9f2b7a1dc8480eacb7f92a43484d7866ba8b89/mxnet_cu102-1.7.0-py2.py3-none-manylinux2014_x86_64.whl`

```
python3.6 -m pip install virtualenv

virtualenv --python='/usr/bin/python3.6' venv      # confira o path do python aqui

source venv/bin/activate

pip install -r requeriments.txt

pip install --upgrade pip

pip install mxnet_cu102-1.7.0-py2.py3-none-manylinux2014_x86_64.whl

python3.6 -c "import nltk; nltk.download('stopwords')"

```

## 3) Testando BERT

```
python3.6 newbert.py --dataset aisopos_ntua_2L --nfolds 5 --foldesp 0
```
Primeira vez ele vai demorar um pouco (porque vai baixar o modelo pre-treinado).

## Observações finais:

> Existem diversos erros que aconteceram já comigo, porém só sei arrumar na prática. Então, peço que sempre que tiverem algum erro, me procurem pois aí podemos criar uma seção de problemas e soluções, o que vai ajudar todo mundo que possuir esse código.

## 4) Voltando Cuda para versão anterior (porque o amiguinho de lab pode precisar dele.)
	
- Siga os passos de limpeza instalação que você percorreu para o CUDA porém na versão que estava antes.

---

# Opcional

Caso você queria instalar o Bert e utiliza-lo no Docker siga os passos abaixo como passo 2.

## 2) Instalação Nvidia-Docker2

### 2.1) Para iniciar vamos garantir que temos a ultima versão do Docker Community edition na máquina:

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt-get update

sudo apt-get install docker-ce

sudo service docker restart
```

### 2.2) Baixar o nvidia-container

```
curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | \
	  sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-container-runtime/ubuntu16.04/amd64/nvidia-container-runtime.list | \
sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list

sudo apt-get update

sudo apt-get install nvidia-container-runtime
```

### 2.3 Baixar o nvidia-docker2

```
sudo apt-get install nvidia-docker2
```
	
### 2.4 Reiniciar a máquina!!!

```
sudo reboot
```	

### 2.5 Conferir a instalação

```
docker run --runtime=nvidia --rm nvidia/cuda nvidia-smi
```

- Veja se esse comando executa o nvidia-smi. Se der ok, está tudo certo até aqui.

### 2.6) **IMPORTANTE**: Verificando requeriments.txt: 

- Se você escolheu outra versão do cuda, edite a ultima linha do arquivo requeriments.txt Deixei como default: mxnet-cu102mkl. (Uma dependencia do BERT)

### 2.7) Build Docker image

- Dentro da pasta que contem o arquivo Dockerfile, digite o seuinte comando:
	
```
docker build -t bertlbd .
```

### 2.8) Executando o Docker

```
docker run --runtime=nvidia --rm --name bertlbd -v /home/waashk/bertLBD:/bertlbd -v /usr/local/cuda:/usr/local/cuda -i -t bertlbd:0.1 /bin/bash
								--name <nome container>
												-v <path fora do docker>:<path dentro do docker>
																					-t <tag/id container>
```