

from inout import *
import gc


import random
import os
import numpy as np
import mxnet as mx
from mxnet import gluon, nd, autograd
import gluonnlp as nlp
from bert import *
from gluonnlp.data import TSVDataset
from glob import glob
from os.path import expanduser
import json
import shutil
from sklearn.metrics import f1_score, precision_score, recall_score, confusion_matrix, accuracy_score, zero_one_loss, confusion_matrix

# plotting libraries
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
import holoviews as hv
from holoviews import opts, dim
hv.extension('bokeh')

import csv
import time
import gzip
import copy

from tqdm import tqdm

# seeding all randomizers
os.environ['PYTHONHASHSEED'] = '0'
np.random.seed(100)
random.seed(100)
mx.random.seed(100)

# use GPU when available otherwise use CPU
ctx = mx.gpu() 

import warnings
warnings.filterwarnings('ignore')

def save_proba(file, X, y):
	with gzip.open(file, 'w') as filout:
		dump_svmlight_file(X, y, filout, zero_based=False)


def print_results(y_true, y_pred):
	print("Error Rate: {:2.2f}".format(100*zero_one_loss(y_true, y_pred)))
	print("Accuracy: {:2.2f}".format(100*accuracy_score(y_true, y_pred)))
	print("F1-Score: {:2.2f}".format(100*f1_score(y_true, y_pred, average="macro")))
	print("Precision: {:2.2f}".format(100*precision_score(y_true, y_pred, average="macro")))
	print("Recall: {:2.2f}".format(100*recall_score(y_true, y_pred, average="macro")))


def arguments():
	parser = argparse.ArgumentParser(description='Bert.')
	parser.add_argument("--dataset", type=str)
	parser.add_argument("--nfolds", type=int)
	parser.add_argument("--foldesp", type=int,default=-1)
	parser.add_argument("--out", type=str, required=True)
	parser.add_argument("--preprocess", type=int, default=0)
	parser.add_argument("--dev", type=int, default=0)
	parser.add_argument("--max_len", type=int, default=150)
	parser.add_argument("--batch_size", type=int, default=32)
	parser.add_argument("--lr",type=float,default=2e-6)
	parser.add_argument("--cleaning_doc", type=bool , default=False)
	parser.add_argument("--doclf", type=int , default=1)
	parser.add_argument("--save_proba", type=int , default=0)
	parser.add_argument("--max_patience", type=int , default=5)
	args = parser.parse_args()

	createPath(f"{args.out}/{args.dataset}")

	random.seed(1608637542)

	if args.foldesp == -1:
		args.folds = list(range(args.nfolds))
	else:
		args.folds = [args.foldesp]

	print(args)
	
	return args

def createPath(p):
	if not path.exists(p):
		os.makedirs(p)


class Dataset(object):
	"""docstring for Dataset"""
	def __init__(self, dataset, nfolds, dev, preprocess, cleaning_doc):
		self.dataset = dataset
		self.nfolds = nfolds 
		self.dev = dev
		self.preprocess = preprocess
		self.cleaning_doc = cleaning_doc

		self.DATAIN = f"datasets/{self.dataset}/"
		self.load_splits()
		self.set_X_and_y_raw()

	def load_splits(self):
		if self.dev == 0:
			self.splits = load_splits_ids(path.join(self.DATAIN,f'split_{self.nfolds}.csv'))
		else:
			self.splits = load_splits_ids_with_validation(path.join(self.DATAIN,f'split_{self.nfolds}.csv'))

	def set_X_and_y_raw(self):
		self.y = LabelEncoder().fit_transform(np.array(list(map(int, readfile(path.join(self.DATAIN, 'score.txt'))))))
		self.X = readfile(path.join(self.DATAIN, 'texts.txt'))

		self.n_classes = len(list(set(self.y)))
		self.all_labels = [str(_) for _ in list(sorted(list(set(self.y))))] 

	def split_and_preprocess(self, f):
		if self.dev == 0:
			train_idx, test_idx = self.splits[f]
			print("Number of Test: ", len(test_idx))
			X_train, y_train = get_array(self.X, train_idx), get_array(self.y, train_idx)
			X_test, y_test = get_array(self.X, test_idx), get_array(self.y, test_idx)

			if self.preprocess == 0:
				X_train, y_train, X_test, y_test, X_val, y_val = prep_data(X_train, y_train, X_test, y_test, cleaning_doc=False, preprocess=True)
			else:
				print("preprocessando")
				X_train, y_train, X_test, y_test, X_val, y_val = prep_data(X_train, y_train, X_test, y_test, cleaning_doc=self.cleaning_doc, preprocess=self.preprocess)

		else:
			train_idx, val_idx, test_idx = self.splits[f]
			
			X_train, y_train = get_array(self.X, train_idx), get_array(self.y, train_idx)
			X_test, y_test =   get_array(self.X, test_idx), get_array(self.y, test_idx)
			X_val, y_val =     get_array(self.X, val_idx), get_array(self.y, val_idx)
			
			if self.preprocess == 1:
				print("preprocessando")
				X_train, y_train, X_test, y_test, X_val, y_val = prep_data2(X_train, y_train, X_test, y_test, X_val, y_val, cleaning_doc=self.cleaning_doc)

		print("Test pos pre-process:", len(X_test))
		#exit()
		return X_train, y_train, X_test, y_test, X_val, y_val
		
	#def get_fold(self, f):
		#self.split_and_preprocess(f)


class BERTMX(object):
	"""docstring for BERTMX"""

	def __init__(self, all_labels, n_classes, 
		max_len=150, batch_size=32, lr=2e-6, max_patience=5, grad_clip=1, log_interval=50):

		self.all_labels = all_labels
		self.n_classes = n_classes

		self.max_len = max_len
		self.batch_size = batch_size
		self.lr = lr
		# train until we fail to beat the current best validation loss for 5 consecutive epochs
		self.max_patience = max_patience
		# gradient clipping value
		self.grad_clip = grad_clip
		# log to screen every 50 batch
		self.log_interval = log_interval

	def fit(self, X_train, y_train, X_val, y_val):

		self.bert_base, self.vocabulary = nlp.model.get_model('bert_12_768_12',
											 dataset_name='book_corpus_wiki_en_uncased',
											 pretrained=True, ctx=ctx, use_pooler=True,
											 use_decoder=False, use_classifier=False)

		# use the vocabulary from pre-trained model for tokenization
		self.tokenizer = tokenization.FullTokenizer(self.vocabulary, do_lower_case=True)
		self.transform = dataset.ClassificationTransform(self.tokenizer, self.all_labels, self.max_len, pair=False)

		data_train = [[X_train[i], str(y_train[i])] for i in range(len(X_train))]
		data_train = gluon.data.SimpleDataset(data_train)

		data_dev = [[X_val[i], str(y_val[i])] for i in range(len(X_val))]
		data_dev = gluon.data.SimpleDataset(data_dev)

		sample_id = np.random.randint(0, len(data_train))
		print('<<<<TEXT>>>>')
		print(data_train[sample_id][0])
		print("<<<<LABEL>>>>")
		print(data_train[sample_id][1])

		data_train = data_train.transform(self.transform)
		data_dev = data_dev.transform(self.transform)

		self.train_dataloader = mx.gluon.data.DataLoader(data_train, batch_size=self.batch_size, shuffle=True, last_batch='keep')
		self.dev_dataloader = mx.gluon.data.DataLoader(data_dev, batch_size=self.batch_size, shuffle=False, last_batch='keep')

		self.model = bert.BERTClassifier(self.bert_base, num_classes=self.n_classes, dropout=0.1)
		# only need to initialize the classifier layer.
		self.model.classifier.initialize(init=mx.init.Normal(0.02), ctx=ctx)
		self.model.hybridize(static_alloc=True)

		# softmax cross entropy loss for classification
		loss_function = gluon.loss.SoftmaxCELoss()
		loss_function.hybridize(static_alloc=True)

		self.metric = mx.metric.Accuracy()

		self.trainer = gluon.Trainer(self.model.collect_params(), 'adam', {'learning_rate': self.lr, 'epsilon': 1e-9})

		# collect all differentiable parameters
		# grad_req == 'null' indicates no gradients are calculated (e.g. constant parameters)
		# the gradients for these params are clipped later
		self.params = [p for p in self.model.collect_params().values() if p.grad_req != 'null']

		train_step = 0
		epoch_id = 0
		best_loss = None
		patience = 0

		t0_train = time.time()

		while True:
			self.metric.reset()
			step_loss = 0
			for batch_id, (token_ids, valid_length, segment_ids, label) in enumerate(self.train_dataloader):
				# load data to GPU
				token_ids = token_ids.as_in_context(ctx)
				valid_length = valid_length.as_in_context(ctx)
				segment_ids = segment_ids.as_in_context(ctx)
				label = label.as_in_context(ctx)

				with autograd.record():
					# forward computation
					out = self.model(token_ids, segment_ids, valid_length.astype('float32'))
					ls = loss_function(out, label).mean()

				# backward computation
				ls.backward()

				# gradient clipping
				grads = [p.grad(c) for p in self.params for c in [ctx]]
				gluon.utils.clip_global_norm(grads, self.grad_clip)

				# parameter update
				self.trainer.step(1)
				step_loss += ls.asscalar()
				self.metric.update([label], [out])
				if (batch_id + 1) % (self.log_interval) == 0:
					print('[Epoch {} Batch {}/{}] loss={:.4f}, lr={:.7f}, acc={:.3f}'
								 .format(epoch_id, batch_id + 1, len(self.train_dataloader),
										 step_loss / self.log_interval,
										 self.trainer.learning_rate, self.metric.get()[1]))

					step_loss = 0
				train_step +=1
			epoch_id+=1
			########################
			#### RUN EVALUATION ####
			########################
			dev_loss = []
			y_true = []
			y_pred = []
			for batch_id, (token_ids, valid_length, segment_ids, label) in enumerate(self.dev_dataloader):
				# load data to GPU
				token_ids = token_ids.as_in_context(ctx)
				valid_length = valid_length.as_in_context(ctx)
				segment_ids = segment_ids.as_in_context(ctx)
				label = label.as_in_context(ctx)
				# get logits and loss value
				out = self.model(token_ids, segment_ids, valid_length.astype('float32'))
				ls = loss_function(out, label).mean()
				dev_loss.append(ls.asscalar())
				probs = out.softmax()
				pred = nd.argmax(probs, axis=1).asnumpy()
				y_true.extend(list(np.reshape(label.asnumpy(), (-1))))
				y_pred.extend(pred)
			dev_loss = np.mean(dev_loss)
			f1 = f1_score(y_true, y_pred, average="macro")
			acc = accuracy_score(y_true, y_pred)
			print('EVALUATION ON DEV DATASET:')
			print('dev mean loss: {:.4f}, f1-score: {:.4f}, accuracy: {:0.4f}'.format(dev_loss, f1, acc))
			if best_loss is None or dev_loss < best_loss:
				best_loss = dev_loss
				print('dev best loss updated: {:.4f}'.format(best_loss))
				patience=0
			else:
				if patience == self.max_patience:
					break
				new_lr = self.trainer.learning_rate/2
				self.trainer.set_learning_rate(new_lr)
				print('patience #{}: reducing the lr to {}'.format(patience, new_lr))
				patience+=1

		self.epoch_id = epoch_id
		self.t0_train = time.time() - t0_train

	def predict(self, X_test, y_test):

		print("predicting...")
		t0_test = time.time()

		self.test_len = len(y_test)

		data_test = [[X_test[i], str(y_test[i])] for i in range(len(X_test))]
		data_test = gluon.data.SimpleDataset(data_test)

		data_test = data_test.transform(self.transform)
		self.test_dataloader = mx.gluon.data.DataLoader(data_test, batch_size=self.batch_size, shuffle=False, last_batch='keep')

		self.y_true = []
		self.y_pred = []
		self.all_proba = None

		for (token_ids, valid_length, segment_ids, label) in tqdm(self.test_dataloader):

			token_ids = token_ids.as_in_context(ctx)
			valid_length = valid_length.as_in_context(ctx)
			segment_ids = segment_ids.as_in_context(ctx)
			label = label.as_in_context(ctx)
			out = self.model(token_ids, segment_ids, valid_length.astype('float32')).softmax()
			if not isinstance(self.all_proba, np.ndarray):
				self.all_proba = copy.copy(out.asnumpy())
			else:
				self.all_proba = np.concatenate((self.all_proba, out.asnumpy()), axis=0) 
			
			pred = nd.argmax(out, axis=1).asnumpy()
			self.y_true.extend(list(np.reshape(label.asnumpy(), (-1))))
			self.y_pred.extend(pred)
		assert len(self.y_true)==len(self.y_pred)

		self.t0_test = time.time() - t0_test
		print("predicted")

	def save_results(self, args, f):

		if args.save_proba:
			print("Saving proba...")
			save_proba(f"{args.out}/{args.dataset}/proba{f}.gz", self.all_proba, self.y_true)

		micro = f1_score(self.y_true, self.y_pred, average="micro")
		macro = f1_score(self.y_true, self.y_pred, average="macro")
		
		with open(f"{args.out}/{args.dataset}/saida", "a") as arq:
			arq.write(f"Micro {f}: {micro}\nMacro {f}: {macro}\n")

		cm = confusion_matrix(self.y_true,self.y_pred).tolist()

		#Saving results in json
		data = {
			"dataset": args.dataset,
			"fold": args.foldesp,
			"method": "bert",
			"max_len": args.max_len,
			"batch_size": self.batch_size,
			"nfolds": args.nfolds,
			"epochs": self.epoch_id,
			"micro": micro,
			"macro": macro,
			"time_train": self.t0_train,
			"time_test": self.t0_test,
			"time_test_avg": self.t0_test/self.test_len,
			"lr": self.lr,
			"grad_clip": self.grad_clip,
			"log_interval": self.log_interval,
			"max_patience": self.max_patience,
			"preprocess": args.preprocess, 
			"dev": args.dev, 
			"cleaning_doc": args.cleaning_doc, 
			"confusion_matrix": cm,
		}

		filename = f"{args.out}/{args.dataset}/out"
		with open(f"{filename}.fold={f}.json", 'w') as outfile:
			json.dump(data, outfile, indent=4)



	def clean(self):
		del self.train_dataloader, self.test_dataloader, self.dev_dataloader, self.model, \
			self.metric, self.trainer, self.params, self.bert_base, self.vocabulary, \
			self.tokenizer, self.transform
		gc.collect()

#python3.6 newbert.py --dataset aisopos_ntua_2L --nfolds 5 --foldesp 0 --out saida
if __name__ == '__main__':
	
	gc.collect()
	args = arguments()

	data = Dataset(args.dataset, 
					args.nfolds, 
					args.dev,
					args.preprocess, 
					args.cleaning_doc)

	for f in args.folds:

		createPath(f"{args.out}/{args.dataset}/{f}")
		X_train, y_train, X_test, y_test, X_val, y_val = data.split_and_preprocess(f)

		bertclf = BERTMX(data.all_labels, 
						data.n_classes,
						max_len=args.max_len,
						batch_size=args.batch_size,
						lr=args.lr,
						max_patience=args.max_patience,
						)

		bertclf.fit(X_train, y_train, X_val, y_val)

		bertclf.predict(X_test, y_test)
		
		bertclf.save_results(args, f)
		bertclf.clean()

		del bertclf
		time.sleep(5)

		
	#stats(args.nfolds, micro_list, macro_list)
